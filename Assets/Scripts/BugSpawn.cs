﻿using UnityEngine;
using System.Collections;

public class BugSpawn : MonoBehaviour {

    public GameObject[] bugs;
    public int amount;
    private Vector3 spawnPoint;
	
	// Update is called once per frame
	void Update () {
        bugs = GameObject.FindGameObjectsWithTag("Bug");
        amount = bugs.Length;

        if (amount != 10)
        {
            InvokeRepeating("SpawnBug", Random.Range(2.0f, 6.0f), Random.Range (3.0f, 6.0f));
        }
	}
    void SpawnBug()
    {
        spawnPoint.x = Random.Range(-5.0f, 5.0f);
        spawnPoint.y = -10.0f;
        spawnPoint.z = 0;

        Instantiate(bugs[UnityEngine.Random.Range(0, bugs.Length - 1)], spawnPoint, Quaternion.identity);
        CancelInvoke();
    }
}
