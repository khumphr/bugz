﻿using UnityEngine;
using System.Collections;

public class Bug : MonoBehaviour {
    public Vector2 velocity;
    public Rigidbody2D rb2D;
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        rb2D.MovePosition(rb2D.position + (velocity * Random.Range(1.0f, 2.0f)) * Time.fixedDeltaTime);
    }
}
